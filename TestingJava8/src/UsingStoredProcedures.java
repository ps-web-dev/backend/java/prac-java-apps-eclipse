import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Scanner;

public class UsingStoredProcedures {
	public static void main(String[] args) {
		try(
				Connection conn = DBUtils.getConnection(DBType.ORADB);
				CallableStatement callableStmt = conn.prepareCall("{call AddNewEmployee(?,?,?,?,?)}");
		)
		{
			int empno;
			String ename, email;
			Date hiredate;
			double salary;
			
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Enter emp id: ");
			empno = Integer.parseInt(sc.nextLine());
			
			System.out.println("Enter emp name: ");
			ename = sc.nextLine();
			
			System.out.println("Enter emp email: ");
			email = sc.nextLine();
			
			System.out.println("Enter hire date: ");
			hiredate = Date.valueOf(sc.nextLine());
			
			System.out.println("Enter salary: ");
			salary = sc.nextDouble();
			
			callableStmt.setInt(1, empno);
			callableStmt.setString(2, ename);
			callableStmt.setString(3, email);
			callableStmt.setDate(4, hiredate);
			callableStmt.setDouble(5, salary);
			
			callableStmt.execute();
			System.out.println("Record added successfully");
			
		}
		catch(SQLException e) {
			DBUtils.showErrorMessage(e);
		}
	}
}
