import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ResultSetScrollingDemo {
	public static void main(String[] args) throws SQLException {
		try (Connection conn = DBUtils.getConnection(DBType.ORADB);
				Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				ResultSet rs = stmt.executeQuery("select * from employees where rownum <= 10")) {
			String format = "%-4s%-20s%-25s%-10f\n";
			rs.beforeFirst();
			System.out.println("First 10 rows : \n");
			while (rs.next()) {
				System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
						rs.getString("Last_Name"), rs.getFloat("Salary"));
			}
			rs.afterLast();
			System.out.println("Last 10 rows : \n");
			while (rs.previous()) {
				System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
						rs.getString("Last_Name"), rs.getFloat("Salary"));
			}

			rs.first();
			System.out.println("First row: \n");
			System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
					rs.getString("Last_Name"), rs.getFloat("Salary"));

			rs.last();
			System.out.println("last row: \n");
			System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
					rs.getString("Last_Name"), rs.getFloat("Salary"));
			
			rs.absolute(4);
			System.out.println("4th row: \n");
			System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
					rs.getString("Last_Name"), rs.getFloat("Salary"));
			
			rs.relative(2);
			System.out.println("6th row will be printed as current cursor position is 4: \n");
			System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
					rs.getString("Last_Name"), rs.getFloat("Salary"));
			
			rs.relative(-3);
			System.out.println("3rd row will be printed as current cursor position is 6: \n");
			System.out.format(format, rs.getString("Employee_ID"), rs.getString("First_Name"),
					rs.getString("Last_Name"), rs.getFloat("Salary"));
			

		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
