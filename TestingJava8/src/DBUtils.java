import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
//	Oracle constants
	private static final String oraUser = "hr"; 
	private static final String oraPass = "hr";
	private static final String oraCS = "jdbc:oracle:thin:@localhost:1521:xe";
	/*
	 *  protocol jdbc
	 *  sub-protocol oracle
	 *  driver thin
	 *  hostname localhost
	 *  port 1521
	 *  oracle service name xe
	 */
	
	
//	MySQL constants
	private static final String mySqlUser = "root";
	private static final String mySqlPass = "1234";
	private static final String mySQLCS = "jdbc:mysql://localhost:3456/world?useSSL=false";
	/*
	 * jdbc : Protocol
	 * msql :
	 * localhost : host server DNS name or IP address
	 * 3456 : mysql port number
	 * world : name of the database
	 * useSSL=false : Do not use SSL(secure) connection. Used to suppress SSL warnings.
	 * */
	
	public static Connection getConnection(DBType dbType) throws SQLException {
		switch (dbType) {
		case ORADB:
			return DriverManager.getConnection(oraCS, oraUser, oraPass);
		case MYSQLDB:
			return DriverManager.getConnection(mySQLCS, mySqlUser, mySqlPass);
		default:
			return null;
		}
	}
	public static void showErrorMessage(SQLException e) {
		System.err.println("Error : " + e.getMessage());
		System.err.println("Error Code : " + e.getErrorCode());
	}
}
